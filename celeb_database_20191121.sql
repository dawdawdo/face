Use Caroline;

SELECT Warning = 'Prevented Catastrophe'

SET NOEXEC ON

IF OBJECT_ID('dbo.Celeb_Image_Pixels_MAILBOX_20191121') IS NOT NULL
	DROP TABLE dbo.Celeb_Image_Pixels_MAILBOX_20191121
GO
CREATE TABLE dbo.Celeb_Image_Pixels_MAILBOX_20191121
(
	ImgPath VARCHAR(260) NULL
	, XPixel VARCHAR(260) NULL
	, YPixel VARCHAR(260) NULL
	, RedValue VARCHAR(260) NULL
	, GreenValue VARCHAR(260) NULL
	, BlueValue VARCHAR(260) NULL
)
-- This file turns out to be way too large for this operation
-- it's about 200GB and 7 billion rows - no thank you!
--BULK INSERT
--	dbo.Celeb_Image_Pixels_MAILBOX_20191121
--FROM
--	'D:\Google_Drive_Sync\Data\Documents\School\UL\Independent_Study_AI_ML\Celeb_Photos_20191114\pixel_data.txt'
--WITH (FIELDTERMINATOR = ',', ROWTERMINATOR = '\n')


IF OBJECT_ID('dbo.Celeb_Image_Pixels') IS NOT NULL
	DROP TABLE dbo.Celeb_Image_Pixels
GO

CREATE TABLE dbo.Celeb_Image_Pixels
(
	ImgPath VARCHAR(260) NOT NULL
	, XPixel SMALLINT NOT NULL
	, YPixel SMALLINT NOT NULL
	, RedValue TINYINT NOT NULL
	, GreenValue TINYINT NOT NULL
	, BlueValue TINYINT NOT NULL
)

IF OBJECT_ID('dbo.Celeb_Attributes_MAILBOX_20191122') IS NOT NULL
	DROP TABLE dbo.Celeb_Attributes_MAILBOX_20191122
GO
CREATE TABLE dbo.Celeb_Attributes_MAILBOX_20191122
(
	[filename] VARCHAR(255) NULL
	, [5_o_Clock_Shadow] VARCHAR(255) NULL
	, Arched_Eyebrows VARCHAR(255) NULL
	, Attractive VARCHAR(255) NULL
	, Bags_Under_Eyes VARCHAR(255) NULL
	, Bald VARCHAR(255) NULL
	, Bangs VARCHAR(255) NULL
	, Big_Lips VARCHAR(255) NULL
	, Big_Nose VARCHAR(255) NULL
	, Black_Hair VARCHAR(255) NULL
	, Blond_Hair VARCHAR(255) NULL
	, Blurry VARCHAR(255) NULL
	, Brown_Hair VARCHAR(255) NULL
	, Bushy_Eyebrows VARCHAR(255) NULL
	, Chubby VARCHAR(255) NULL
	, Double_Chin VARCHAR(255) NULL
	, Eyeglasses VARCHAR(255) NULL
	, Goatee VARCHAR(255) NULL
	, Gray_Hair VARCHAR(255) NULL
	, Heavy_Makeup VARCHAR(255) NULL
	, High_Cheekbones VARCHAR(255) NULL
	, Male VARCHAR(255) NULL
	, Mouth_Slightly_Open VARCHAR(255) NULL
	, Mustache VARCHAR(255) NULL
	, Narrow_Eyes VARCHAR(255) NULL
	, No_Beard VARCHAR(255) NULL
	, Oval_Face VARCHAR(255) NULL
	, Pale_Skin VARCHAR(255) NULL
	, Pointy_Nose VARCHAR(255) NULL
	, Receding_Hairline VARCHAR(255) NULL
	, Rosy_Cheeks VARCHAR(255) NULL
	, Sideburns VARCHAR(255) NULL
	, Smiling VARCHAR(255) NULL
	, Straight_Hair VARCHAR(255) NULL
	, Wavy_Hair VARCHAR(255) NULL
	, Wearing_Earrings VARCHAR(255) NULL
	, Wearing_Hat VARCHAR(255) NULL
	, Wearing_Lipstick VARCHAR(255) NULL
	, Wearing_Necklace VARCHAR(255) NULL
	, Wearing_Necktie VARCHAR(255) NULL
	, Young VARCHAR(255) NULL
)
GO

BULK INSERT
	dbo.Celeb_Attributes_MAILBOX_20191122
FROM
	'D:\Google_Drive_Sync\Data\Documents\School\UL\Independent_Study_AI_ML\Celeb_Photos_20191114\list_attr_celeba_csv_correction.txt'
WITH (FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, BATCHSIZE = 100000)

IF OBJECT_ID('dbo.Celeb_Attributes') IS NOT NULL
	DROP TABLE dbo.Celeb_Attributes
GO
CREATE TABLE dbo.Celeb_Attributes
(
	[filename] VARCHAR(260) NOT NULL PRIMARY KEY
	, [5_o_Clock_Shadow] BIT NOT NULL
	, Arched_Eyebrows BIT NOT NULL
	, Attractive BIT NOT NULL
	, Bags_Under_Eyes BIT NOT NULL
	, Bald BIT NOT NULL
	, Bangs BIT NOT NULL
	, Big_Lips BIT NOT NULL
	, Big_Nose BIT NOT NULL
	, Black_Hair BIT NOT NULL
	, Blond_Hair BIT NOT NULL
	, Blurry BIT NOT NULL
	, Brown_Hair BIT NOT NULL
	, Bushy_Eyebrows BIT NOT NULL
	, Chubby BIT NOT NULL
	, Double_Chin BIT NOT NULL
	, Eyeglasses BIT NOT NULL
	, Goatee BIT NOT NULL
	, Gray_Hair BIT NOT NULL
	, Heavy_Makeup BIT NOT NULL
	, High_Cheekbones BIT NOT NULL
	, Male BIT NOT NULL
	, Mouth_Slightly_Open BIT NOT NULL
	, Mustache BIT NOT NULL
	, Narrow_Eyes BIT NOT NULL
	, No_Beard BIT NOT NULL
	, Oval_Face BIT NOT NULL
	, Pale_Skin BIT NOT NULL
	, Pointy_Nose BIT NOT NULL
	, Receding_Hairline BIT NOT NULL
	, Rosy_Cheeks BIT NOT NULL
	, Sideburns BIT NOT NULL
	, Smiling BIT NOT NULL
	, Straight_Hair BIT NOT NULL
	, Wavy_Hair BIT NOT NULL
	, Wearing_Earrings BIT NOT NULL
	, Wearing_Hat BIT NOT NULL
	, Wearing_Lipstick BIT NOT NULL
	, Wearing_Necklace BIT NOT NULL
	, Wearing_Necktie BIT NOT NULL
	, Young BIT NOT NULL
)
GO

INSERT
	dbo.Celeb_Attributes
SELECT
	[filename] = REPLACE([filename], 'jpg', 'png')
	, CASE WHEN [5_o_Clock_Shadow] = '1' THEN 1 ELSE 0 END
	, CASE WHEN Arched_Eyebrows = '1' THEN 1 ELSE 0 END
	, CASE WHEN Attractive = '1' THEN 1 ELSE 0 END
	, CASE WHEN Bags_Under_Eyes = '1' THEN 1 ELSE 0 END
	, CASE WHEN Bald = '1' THEN 1 ELSE 0 END
	, CASE WHEN Bangs = '1' THEN 1 ELSE 0 END
	, CASE WHEN Big_Lips = '1' THEN 1 ELSE 0 END
	, CASE WHEN Big_Nose = '1' THEN 1 ELSE 0 END
	, CASE WHEN Black_Hair = '1' THEN 1 ELSE 0 END
	, CASE WHEN Blond_Hair = '1' THEN 1 ELSE 0 END
	, CASE WHEN Blurry = '1' THEN 1 ELSE 0 END
	, CASE WHEN Brown_Hair = '1' THEN 1 ELSE 0 END
	, CASE WHEN Bushy_Eyebrows = '1' THEN 1 ELSE 0 END
	, CASE WHEN Chubby = '1' THEN 1 ELSE 0 END
	, CASE WHEN Double_Chin = '1' THEN 1 ELSE 0 END
	, CASE WHEN Eyeglasses = '1' THEN 1 ELSE 0 END
	, CASE WHEN Goatee = '1' THEN 1 ELSE 0 END
	, CASE WHEN Gray_Hair = '1' THEN 1 ELSE 0 END
	, CASE WHEN Heavy_Makeup = '1' THEN 1 ELSE 0 END
	, CASE WHEN High_Cheekbones = '1' THEN 1 ELSE 0 END
	, CASE WHEN Male = '1' THEN 1 ELSE 0 END
	, CASE WHEN Mouth_Slightly_Open = '1' THEN 1 ELSE 0 END
	, CASE WHEN Mustache = '1' THEN 1 ELSE 0 END
	, CASE WHEN Narrow_Eyes = '1' THEN 1 ELSE 0 END
	, CASE WHEN No_Beard = '1' THEN 1 ELSE 0 END
	, CASE WHEN Oval_Face = '1' THEN 1 ELSE 0 END
	, CASE WHEN Pale_Skin = '1' THEN 1 ELSE 0 END
	, CASE WHEN Pointy_Nose = '1' THEN 1 ELSE 0 END
	, CASE WHEN Receding_Hairline = '1' THEN 1 ELSE 0 END
	, CASE WHEN Rosy_Cheeks = '1' THEN 1 ELSE 0 END
	, CASE WHEN Sideburns = '1' THEN 1 ELSE 0 END
	, CASE WHEN Smiling = '1' THEN 1 ELSE 0 END
	, CASE WHEN Straight_Hair = '1' THEN 1 ELSE 0 END
	, CASE WHEN Wavy_Hair = '1' THEN 1 ELSE 0 END
	, CASE WHEN Wearing_Earrings = '1' THEN 1 ELSE 0 END
	, CASE WHEN Wearing_Hat = '1' THEN 1 ELSE 0 END
	, CASE WHEN Wearing_Lipstick = '1' THEN 1 ELSE 0 END
	, CASE WHEN Wearing_Necklace = '1' THEN 1 ELSE 0 END
	, CASE WHEN Wearing_Necktie = '1' THEN 1 ELSE 0 END
	, CASE WHEN Young = '1' THEN 1 ELSE 0 END
FROM
	dbo.Celeb_Attributes_MAILBOX_20191122

IF OBJECT_ID('dbo.Celeb_Face_Queries') IS NOT NULL
	DROP TABLE dbo.Celeb_Face_Queries
GO
CREATE TABLE dbo.Celeb_Face_Queries
(
	FaceFileName VARCHAR(260) NOT NULL
		CONSTRAINT PK_Celeb_Face_Queries PRIMARY KEY
	, FaceDescription VARCHAR(100) NOT NULL
		CONSTRAINT UC_Celeb_Face_Queries_FaceDescription UNIQUE
	, FaceQuery VARCHAR(1000) NOT NULL
		CONSTRAINT UC_Celeb_Face_Queries_FaceQuery UNIQUE
	, FileStatus CHAR(3) NOT NULL
		CONSTRAINT DF_Celeb_Face_Queries_FileStatus DEFAULT('RDY')
)
GO
INSERT
	dbo.Celeb_Face_Queries
	(FaceFileName, FaceDescription, FaceQuery, FileStatus)
VALUES
	('avg_face.png', 'Complete average across all images', 'SELECT [filename] FROM dbo.Celeb_Attributes', 'CMP')

INSERT
	dbo.Celeb_Face_Queries
	(FaceFileName, FaceDescription, FaceQuery)
VALUES
	('female_face.png', 'Average female face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE Male = 0')
	, ('male_face.png', 'Average male face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE Male = 1')
	, ('necklaced_face.png', 'Average necklaced face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE Wearing_Necklace = 1')
	, ('hatted_face.png', 'Average hatted face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE Wearing_Hat = 1')
	, ('attractive_face.png', 'Average attractive face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE Attractive = 1')
	, ('unattractive_face.png', 'Average unattractive face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE Attractive = 0')
	, ('bearded_face.png', 'Average bearded face', 'SELECT [filename] FROM dbo.Celeb_Attributes WHERE No_Beard = 0')


SET NOEXEC OFF

-- Test
SELECT * FROM dbo.Celeb_Face_Queries


