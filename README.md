# face

face - [f]acial [a]ggregator of [ce]lebrities - operates on the Large-scale CelebFaces Attributes (CelebA) Dataset
(http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html) and generates the average face based on some basic annotations.

# Requirements
python 2 or 3
pathlib
PIL
pypyodbc

# Questions
Contact stephen.abrams@gmail.com