# Standard Modules
from collections import defaultdict
import logging as log
from math import floor

# Third-Party Modules
from pathlib import Path
from PIL import Image
import pypyodbc as sql

def main():

    fmt = '%(asctime)-15s: %(message)s'
    log.basicConfig(level=log.INFO, format=fmt)
    
    log.info('Initializing average image generator...')

    img_root = Path(r'D:\Google_Drive_Sync\Data\Documents\School\UL'
                r'\Independent_Study_AI_ML\Celeb_Photos_20191114'
                r'\pngs')
    out_img_pth = Path(r'D:\Google_Drive_Sync\Data\Documents\School\UL'
                r'\Independent_Study_AI_ML\Celeb_Photos_20191114'
                r'\output')
    
    width, height = 178, 218


    log.info('Collecting unprocessed images...')
    conn = sql.connect("DRIVER={SQL Server};Server=MSI\GLADOS;DATABASE=Caroline;Trusted_Connection=Yes")
    crsr = conn.cursor()
    img_qry = r"""SELECT FaceFileName, FaceQuery
    FROM dbo.Celeb_Face_Queries
    WHERE FileStatus = 'RDY'
    """
    res_qry = r"""
    UPDATE dbo.Celeb_Face_Queries
    SET FileStatus = ?
    WHERE FaceFileName = ?
    """
    crsr.execute(img_qry)
    qry_list = crsr.fetchall()
    log.info('Processing images...')

    for (out_img, qry) in qry_list:

        try:
            # The img_data identifier stores the sum of all RGB values for
            # each pixel in the 178x218 array. At the end of the script, its
            # values are averaged over the number of images processed.
            log.debug('Initializing data array with empty RGB values...')
            img_data = defaultdict(tuple)
            for x in range(0, width):
                for y in range(0, height):
                    img_data[(x, y)] = (0, 0, 0)

            log.info('Collecting file names...')
            crsr.execute(qry)
            filenames = [r[0] for r in crsr.fetchall()]

            log.info('Found %i files. Collecting pixel data...' % len(filenames))
            for idx, fl in enumerate(filenames):

                im = Image.open(img_root / fl)
                pix = im.load()
                log.debug('Processing %s... ' % fl)
                for x in range(0, im.size[0]):
                    for y in range(0, im.size[1]):
                        rgb = pix[x, y]
                        img_data[(x, y)] = tuple(map(sum, zip(img_data[(x, y)], rgb)))

                if (1 + idx) % 4000 == 0:
                    log.info('Processed %i images...' % (1 + idx))

            log.info('Processed %i images' % (idx+1))

            log.info('Generating average values for each pixel...')
            for x in range(0, width):
                    for y in range(0, height):
                        pix[x, y] = tuple([floor(val / (1 + idx)) for val in img_data[(x,y)]])
        
            log.info('Saving result to %s' % out_img_pth)    
            im.save(out_img_pth / out_img)
        except Exception as err:
            log.info('Something went wrong :(: %s' % repr(err))
            status = 'FLD'
        else:
            log.info('Finished saving %s!' % out_img)
            status = 'CMP'
        finally:
            crsr.execute(res_qry, (status, out_img))
            conn.commit()            

if __name__ == '__main__': main()


